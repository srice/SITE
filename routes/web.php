<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ["as" => "home", "uses" => "HomeController@index"]);
Route::get('pk', ["as" => "pk", "uses" => "HomeController@pk"]);

Route::group(["prefix" => "solution", "namespace" => "Solution"], function (){
    Route::get('logiciel', ["as" => "solution.logiciel", "uses" => "SolutionController@logiciel"]);
    Route::get('billetterie', ["as" => "solution.billetterie", "uses" => "SolutionController@billetterie"]);
    Route::get('apps', ["as" => "solution.apps", "uses" => "SolutionController@apps"]);
    Route::get('compta', ["as" => "solution.compta", "uses" => "SolutionController@compta"]);
});

Route::group(["prefix" => "blog", "namespace" => "Blog"], function (){
    Route::get('/', ["as" => "blog.index", "uses" => "BlogController@index"]);
    Route::get('{id}', ["as" => "blog.post", "uses" => "BlogController@post"]);
});

Route::group(["prefix" => "partenaire", "namespace" => "Partenaire"], function (){
    Route::get('/', ["as" => "partenaire.index", "uses" => "PartenaireController@index"]);
});

Route::group(["prefix" => "contact", "namespace" => "Contact"], function (){
    Route::get('/', ["as" => "contact.index", "uses" => "ContactController@index"]);
    Route::post('/', ["as" => "contact.store", "uses" => "ContactController@store"]);
});

Route::group(["prefix" => "support", "namespace" => "Support"], function (){
    Route::get('/', ["as" => "support.index", "uses" => "SupportController@index"]);
    Route::get('/nous-contacter', ["as" => "support.contact", "uses" => "SupportController@contact"]);
});

Route::group(["prefix" => "guide", "namespace" => "Guide"], function (){
    Route::get('/', ["as" => "guide.index", "uses" => "GuideController@index"]);
});

Route::group(["prefix" => "status", "namespace" => "Status"], function (){
    Route::get("/", ["as" => "status.index", "uses" => "StatusController@index"]);
    Route::get("{id}", ["as" => "status.show", "uses" => "StatusController@show"]);
});

Route::group(["prefix" => "modules", "namespace" => "Modules"], function (){
    Route::get('/', ["as" => "modules.index", "uses" => "ModuleController@index"]);
    Route::get('{id}', ["as" => "modules.show", "uses" => "ModuleController@show"]);
});

Route::get('test', 'TestController@test');
