@extends('template')
@section("title")
    Nous Contacter
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.html">{{ env('APP_NAME') }}</a> </li>
                <li class="active">@yield("title")</li>
            </ul>
        </div>
    </div><!-- .breadcrumb-box -->

    <section id="main">
        <header class="page-header">
            <div class="container">
                <h1 class="title">@yield("title")</h1>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <div class="content col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 contact-info bottom-padding">
                            <address>
                                <div class="title">Adresse</div>
                                22 Rue Maryse Bastié<br>
                                85100 Les Sables d'Olonne<br>
                                FRANCE
                            </address>
                            <div class="row">
                                <!--<address class="col-sm-6 col-md-6">
                                    <div class="title">Phones</div>
                                    <div>Support: +777 (100) 1234</div>
                                    <div>Sales manager: +777 (100) 4321</div>
                                    <div>Director: +777 (100) 1243</div>
                                </address>-->
                                <address class="col-sm-6 col-md-6">
                                    <div class="title">Email Addresses</div>
                                    <div>Service Technique: <a href="mailto:support@srice.eu">support@srice.eu</a></div>
                                    <div>Service Commecial: <a href="mailto:manager@example.com">contact@srice.eu</a></div>
                                    <div>Service Formation: <a href="mailto:chief@example.com">helpme@srice.eu</a></div>
                                </address>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 bottom-padding">
                            <!--<form id="contactform" class="form-box register-form contact-form" method="POST">
                                <h3 class="title">Quick Contact</h3>
                                <div id="success"></div>
                                <label>Name: <span class="required">*</span></label>
                                <input class="form-control" type="text" name="name">
                                <label>Email Address: <span class="required">*</span></label>
                                <input class="form-control" type="email" name="email">
                                <label>Telephone:</label>
                                <input class="form-control" type="text" name="phone">
                                <label>Comment:</label>
                                <textarea class="form-control" name="comment"></textarea>
                                <div class="clearfix"></div>
                                <div class="buttons-box clearfix">
                                    <button id="submit" class="btn btn-default">Submit</button>
                                    <span class="required"><b>*</b> Required Field</span>
                                </div><!-- .buttons-box -->
                            <!--</form>-->
                            <div class="g-recaptcha"
                                 data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                            </div>
                            @if(Session::has('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                </div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger">
                                    {{ Session::get('error') }}
                                </div>
                            @endif
                            {{ Form::open(["route" => "contact.store", "class" => "form-box register-form contact-form"]) }}

                            {{ Form::label('name', 'Votre nom') }}
                            {{ Form::text('name', null, ["class" => "form-control"]) }}

                            {{ Form::label('email', 'Votre adresse email') }}
                            {{ Form::email('email', null, ["class" => "form-control"]) }}

                            {{ Form::label('tel', 'Votre numéro de téléphone') }}
                            {{ Form::tel('tel', null, ["class" => "form-control"]) }}

                            {{ Form::label('comment', 'Votre message') }}
                            {{ Form::textarea('comment', null, ["class" => "form-control"]) }}
                            <div class="buttons-box clearfix">
                                <button type="submit" class="btn btn-default"><i class="fa fa-envelope"></i> Envoyer</button>
                                <span class="required"><b>*</b> Required Field</span>
                            </div><!-- .buttons-box -->
                            {{ Form::close() }}
                        </div>
                        <div class="map-box col-sm-12 col-md-12">
                            <div
                                    style="height: 276px;"
                                    class="map-canvas"
                                    data-zoom="16"
                                    data-lat="46.4931523"
                                    data-lng="-1.7630728999999974"
                                    data-title="Les Sables d'Olonne"
                                    data-content=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </section><!-- #main -->
@stop
@section("footer_scripts")
    <script src='https://www.google.com/recaptcha/api.js?render=6Ld0mX0UAAAAAK5TyHwYJFR0QZZXPZvYFKzyu2ME'></script>
@stop    