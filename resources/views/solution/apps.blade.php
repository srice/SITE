@extends('template')
@section("title")
    SITE INTERNET & APPLI CSE
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route("home") }}">{{ env('APP_NAME') }}</a> </li>
                <li>Solution</li>
                <li class="active">@yield("title")</li>
            </ul>
        </div>
    </div>
    <section id="main">
        <div class="full-width-box">
            <div class="fwb-bg paralax" data-stellar-background-ratio="-0.01" style="background-image: url('/assets/custom/images/1511.jpg')"><div class="overlay"></div></div>
            <div class="container">
                <h1 class="title white text-center"><i class="livicon" data-name="cellphone" data-size="90" data-color="#ffffff"></i> SITE INTERNET & APPLI CE, CSE</h1>
            </div>
        </div>
        <div class="container">
            <div class="content-block bottom-padding frame-shadow-raised text-center">
                <strong class="lead">Augmenter la valorisation de votre CE, CSE avec nos <span class="text-danger">outils de communications performant</span></strong>
                <p class="text-small">Valorisez vos actions, optimisez votre communication et renforcez la proximité auprès des salariés. Découvrez toutes nos solutions pour diffuser la bonne information au bon moment.</p>
            </div>
        </div>
        <div class="container">
            <div class="title-box text-center">
                <h1 class="title">Site internet CE, CSE, appli mobile : <br>faites entrer votre comité dans <span class="text-danger">l’ère du digital</span></h1>
            </div>
        </div>
        <div class="container">
            <div class="service">
                <a href="#" class="icon bg"><i class="fa fa-cogs"></i></a>
                <h6 class="title text-uppercase">Des outils et des services interactifs qui permettent à votre CSE de:</h6>
                <div class="text-small">
                    <ul>
                        <li><strong>Maintenir un contact</strong> permanent avec vos salariés</li>
                        <li><strong>Valoriser</strong> et <strong>Renforcer</strong> votre image d'Elue CE, CSE</li>
                        <li><strong>Dynamiser vos actions CE, CSE</strong> (Newsletter, Sondage, Jeux On-line, etc...)</li>
                        <li><strong>Optimiser la diffusion</strong> et la transmission de messages (PV de réunion, actualités, etc...)</li>
                        <li><strong>Développer des Services</strong> en ligne (commandes de billetterie, de chèque vacance, réservation de voyage, paiement, etc...)</li>
                        <li>Proposer des espaces de <strong>dialogue et d'échange</strong> (Forum, chat, etc...)</li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="service">
                <a href="#" class="icon bg"><i class="fa fa-desktop"></i></a>
                <h6 class="title text-uppercase">Un site entièrement personnalisable en quelque clic pour mettre en valeur vos actions</h6>
                <div class="text-small">
                    <p>Afin de présenter efficacement vos activités, nous vous proposons un site internet dont les <strong>fonctionnalités ont été conçues spécialement pour les CSE</strong></p>
                    <ul>
                        <li>Formats de contenus prédéfinis, à sélectionner par simple glisser/déposer</li>
                        <li>Photothèque, diaporama, trombinoscope des élus</li>
                        <li>Calendrier des événements, formulaire de contact, moteur de recherche</li>
                        <li>Offres du CSE</li>
                        <li>Gestion de newsletter (création par modèle et gestion des envois)</li>
                        <li>Mise à jour automatique de contenus par flux RSS/API (actualité, météo, offres billetterie Srice, ….)</li>
                    </ul>
                    <p>En <strong>totale interaction avec les autres solutions SRICE</strong>, vous avez la possibilité de mettre en ligne tout ou partie de vos offres</p>
                    <ul>
                        <li>Location de vacances, cadeaux, chèques vacances (ANCV), etc..., les salariés peuvent ainsi directement réserver et/ou payer en ligne. La mise à jour des stocks remonte automatiquement dans votre logiciel de gestion CE, CSE.</li>
                        <li>Grâce aux différents flux, les offres de billetterie & shopping animent votre site internet. Les Salariés accèdent directement à la plateforme de billetterie de SRICE sans avoir à ce connecter.</li>
                    </ul>
                    <p>A partir de votre cahier des charges, L'équipe technique de SRICE peut également réaliser votre site internet sur-mesure.</p>
                </div>
            </div>
            <hr>
            <div class="service">
                <a href="#" class="icon bg"><i class="fa fa-mobile-phone"></i></a>
                <h6 class="title text-uppercase">Une application mobile CE, CSE pour communiquer en temps réel avec nous, vous et vos salariés.</h6>
                <div class="text-small">
                    <p>Accompagner les salariés partout où il se trouvent: votre appli mobile SRICE MonCSE leurs transmet en temps réel les informations importantes et donne un image <strong>nouvelle</strong> et <strong>innovante à votre CE, CSE.</strong> </p>
                    <p>Avec votre Application de CE, CSE:</p>
                    <ul>
                        <li>Envoyer un message à tout moment à vos salariés (notif PUSH)</li>
                        <li>Elaborer des campagnes de communication</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@stop
@section("footer_scripts")

@stop    