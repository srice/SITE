@extends('template')
@section("title")
    BILLETTERIE CSE & SHOPPING
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ env('APP_NAME') }}</a> </li>
                <li>Solution</li>
                <li class="active">@yield("title")</li>
            </ul>
        </div>
    </div>
    <section id="main">
        <div class="full-width-box">
            <div class="fwb-bg" data-stellar-background-ratio="0" style="background-image: url('/assets/custom/images/banner_partenaire.png')"><div class="overlay"></div></div>
            <div class="container">
                <img class="center-block" src="/assets/custom/images/logo-long.png" alt="">
                <h1 class="title white text-center"><i class="livicon" data-name="shopping-cart" data-size="90" data-color="#ffffff"></i> BILLETTERIE CSE & SHOPPING</h1>
                <div class="text-center">
                    <a href="https://francebilletreduc.shop/" class="btn btn-lg btn-white">Découvez notre billetterie</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="content-block bottom-padding frame-shadow-raised text-center">
                <strong class="lead">La billetterie de SRICE est une plateforme pour les CE, CSE et les salariés.</strong>
                <p class="text-small">Simplifier la gestion de votre billetterie et <strong>participer au pouvoir d'achat des salariés de votre entreprise.</strong></p>
                <h6 class="text-danger">Plateforme intégrée au logiciel SRICE & au site internet de votre comité</h6>
            </div>
        </div>
        <div class="container">
            <div class="service">
                <a href="#" class="icon bg"><i class="fa fa-gift"></i></a>
                <h6 class="title text-uppercase">Des avantages cumuler</h6>
                <div class="text-small">
                    <ul>
                        <li>Vous proposer à vos salariés leurs <strong>Spectacle, Concert et Sortie préférées à des prix remisés toutes l'années.</strong></li>
                        <li><strong>Vous souhaitez leur offrir plus ?</strong> Ajoutez votre participation CE, CSE relatif au tarifs négociés dans la billetterie.</li>
                    </ul>
                    <p class="text-danger">La billetterie SRICE est faite pour vous et nous nous occupons du reste.</p>
                    <p>D'un seul clic, les salariés bénéficient des tarif de la billetterie et cumules déja aux avantages déjà offerts par votre CSE.</p>
                </div>
            </div>
            <hr>
            <div class="service">
                <a href="#" class="icon bg"><i class="fa fa-desktop"></i></a>
                <h6 class="title text-uppercase">Une utilisation simplifier</h6>
                <div class="text-small">
                    <p><strong>Le parcours de l'utilisateur est simple et intuitif,</strong> il permet au salarié en un coup d'oeil de visualiser l'ensemble de ses avantages et d'effectuer ses commandes.</p>
                    <p>La billetterie reste toujours à porter de votre main grace à <strong>votre smartphone, tablette ou ordinateur</strong> pour une consultation <span class="text-danger">7J/7 24H/24</span></p>
                </div>
            </div>
            <hr>
            <div class="service">
                <a href="#" class="icon bg"><i class="fa fa-users"></i></a>
                <h6 class="title text-uppercase">Personnalisation de votre profil</h6>
                <div class="text-small">
                    <p>L'agrégateur de la billetterie de SRICE permet au salarié de selectionner les produits et les newsletters qui lui correspond.<br>Notre Objectif,
                        lui proposer l'offre la plus proche de ses besoins. Il lui suffit de les formuler.</p>
                    <p>
                        <strong>Quand il veut et comme il veut,</strong> il peut ainsi découvrir les offres exclusive de la billetterie et réaliser des achats malin et les recevoir directement par mail ou sur son mobile.
                    </p>
                    <span class="text-danger">C'est simple pour lui, alors c'est simple pour vous.</span>
                </div>
            </div>
        </div>
    </section>
@stop
@section("footer_scripts")

@stop    