@extends('template')
@section("title")
    Assistance Juridique et comptable
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">@yield("APP_SITE")</a> </li>
                <li>Solution</li>
                <li class="active"> @yield("namePage")</li>
            </ul>
        </div>
    </div>
    <section id="main">
        <div class="full-width-box">
            <div class="fwb-bg paralax" data-stellar-background-ratio="-0.01" style="background-image: url('views/assets/custom/images/assistance.jpg')"><div class="overlay"></div></div>
            <div class="container">
                <h1 class="title white text-center"><i class="livicon" data-name="balance" data-size="90" data-color="#ffffff"></i> ASSISTANCE JURIDIQUE & COMPTABLE</h1>
            </div>
        </div>
        <div class="container">
            <div class="tabs">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#formation" data-toggle="tab"><i class="fa fa-book"></i> Formations</a>
                    </li>
                    <li>
                        <a href="#comptabilite" data-toggle="tab"><i class="fa fa-euro"></i> Comptabilité</a>
                    </li>
                </ul>
                <div class="tabs-content">
                    <div class="tab-pane active fade in" id="formation">
                        <div class="content-block frame-shadow-raised">
                            <h6 class="title text-uppercase">Un large panel de formations aux élues</h6>
                            <strong>Public visée:</strong> Membres du C.E<br>
                            <strong>Objectif:</strong> Appréhender les rôles et les missions des membres du C.E - Connaître les réglementations opposables au C.E - S'assurer des moyens d'exercice du mandat des élus.<br>
                            <strong>Description:</strong><br>
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td>Durée</td>
                                    <td class="text-left">3 Jours</td>
                                </tr>
                                <tr>
                                    <td>Objectifs</td>
                                    <td class="text-left">
                                        <ul>
                                            <li>Appréhender  les rôles et les missions des membres du C.E</li>
                                            <li>Connaître les réglementations opposables au C.E</li>
                                            <li>S’assurer des  moyens d'exercice du mandat des élus</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Contenue</td>
                                    <td class="text-left">
                                        <ol>
                                            <li>
                                                <strong>Le comité d'entreprise et les  institutions représentatives du personnel</strong>
                                                <ul>
                                                    <li>les différents acteurs des relations sociales (CE, DP, CHSCT, DS)</li>
                                                    <li>les enjeux liés au mandats du CE</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <strong>Organisation  d’un CE</strong>
                                                <ul>
                                                    <li>Le  règlement intérieur, le penser, l’écrire , le confronter au textes et lois.</li>
                                                    <li>Élection du  secrétaire et  du  trésorier</li>
                                                    <li>Rôle  et intérêt des commission</li>
                                                    <li>lesquelles mettre en place ?</li>
                                                    <li>La vie du C.E au quotidien  (local….)</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <strong>L’exercice du  mandat</strong>
                                                <ul>
                                                    <li>Notion d’heures de délégation</li>
                                                    <li>Notion de  contrôle de l'employeur</li>
                                                    <li>Le droit à la formation (nouvelles orientations 2015)</li>
                                                    <li>liberté de déplacement des élus (dans quel cadre)</li>
                                                    <li>obligation de discrétion : Ce que dit la loi</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <strong>Gérer ses budgets</strong>
                                                <ul>
                                                    <li>budget de fonctionnement, budget des activités sociales et culturelles : de quoi dispose le CE ?</li>
                                                    <li>Utilisation rationnelle du  budget de fonctionnement ?</li>
                                                    <li>Les principes comptables incontournables</li>
                                                    <li>Les obligations comptables et de gestion du CE</li>
                                                    <li>rapport d'activité et de gestion</li>
                                                </ul>
                                                <p><strong>Notre point fort :</strong> Pratique sur logiciel de gestion et de comptabilité développé par le groupe.</p>
                                            </li>
                                            <li>
                                                <strong>Les réunions</strong>
                                                <ul>
                                                    <li>Ordre du jour</li>
                                                    <li>Les participants aux réunions</li>
                                                    <li>Cas pratique : Déroulement type d’une réunion</li>
                                                    <li>les informations et consultations du CE</li>
                                                    <li>le procès-verbal</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <strong>Connaître les attributions économiques du CE  et le recours aux experts</strong>
                                                <ul>
                                                    <li>les  droits  du CE ?</li>
                                                    <li>L’information due aux C.E</li>
                                                    <li>Le droit d’alerte</li>
                                                    <li>expert-comptable, , expert indépendant, juristes…..</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <strong>Gestion des  activités sociales et culturelles</strong>
                                                <ul>
                                                    <li>Étude des différentes actions sociales</li>
                                                    <li>Étude des normes ACOSS</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <strong>Responsabilité du CE</strong>
                                                <ul>
                                                    <li>responsabilité civile et pénale du CE et de ses membres</li>
                                                    <li>vérifier que le CE est bien assuré</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <strong>Les différents moyens de communication du C.E</strong>
                                                <ul>
                                                    <li>supports papier…, Internet, messageries électroniques…..le cadre de lois</li>
                                                    <li>Les frais de communication : quel budget utiliser ?  attention danger !</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <strong>La protection des membres du CE</strong>
                                                <ul>
                                                    <li>La protection des membres du CE contre le licenciement</li>
                                                    <li>Le délit d'entrave</li>
                                                </ul>
                                            </li>
                                        </ol>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="comptabilite">
                        <div class="content-block frame-shadow-raised">
                            <h6 class="title text-uppercase">Une Comptabilité Transparent</h6>
                            <p class="text-info"><i>Les nouvelles lois sur la gestion comptable des CE vont changer la donne et pousser tous les Comités d’entreprise d’une certaine taille (plus de 153.000 € de revenus) à tenir une comptabilité selon les mêmes normes comptables qu’une entreprise (obligation de faire un inventaire des stocks, des immobilisations…), obligation de présenter chaque année à partir du 01/01/2015 : un bilan, un compte de résultat, une annexe, un rapport de gestion.</i></p>
                        </div>
                        <p>Les revenus sont composés de la subvention des œuvres sociales, de celle du budget de fonctionnement, des revenus financiers, du remboursement de l’assurance en responsabilité civile… Par contre, pour le calcul du seuil de 153.000 € les paiements des salariés pour l’obtention d’œuvres sociale ne doit être pris en compte (par exemple la participation financière des salariés pour acheter des places de cinéma ou des chèques vacances).</p>
                        <p>Il vous faut donc faire la somme de ces revenus et voir si vous êtes au dessus de 153.000 €.</p>
                        <p>La loi va aussi imposer <strong>l’intervention d’un expert comptable pour le CE</strong> de plus de 153.000 € de revenus. </p>
                        <p>Si votre CE à un chiffre d'affaire égal ou supérieur à 153 000€, notre entreprise et groupe de partenaire travail avec des cabinets d'expertise comptable spécialiser dans la formation des élues et la gestion et comptabilité de Comité d'Entreprise.</p>
                        <h5 class="text-info">Les Petits Comités d'entreprise</h5>
                        <p>Ainsi en dessous de 153.000 €, vous pouvez présenter une comptabilité dite de trésorerie (ultra simplifiée).<br>Que nous pouvons également effectuer pour vous.</p>
                        <h5 class="text-info">Comment déterminer votre seuil</h5>
                        <p> Pour déterminer le seuil des 153.000 €, il convient de se référer à l’article R2323-34 du Code du Travail :</p>
                        <div class="well-lg">
                            <i>
                                Les ressources du comité d’entreprise en matière d’activités sociales et culturelles sont constituées par :<br>
                                1° Les sommes versées par l’employeur pour le fonctionnement des institutions sociales de l’entreprise qui ne sont pas légalement à sa charge, à l’exclusion des sommes affectées aux retraités (budget des oeuvres sociales ainsi que le budget de fonctionnement) ;<br>
                                2° Les sommes précédemment versées par l’employeur aux caisses d’allocations familiales et organismes analogues, pour les institutions financées par ces caisses et qui fonctionnent au sein de l’entreprise ;<br>
                                3° Le remboursement obligatoire par l’employeur des primes d’assurances dues par le comité d’entreprise pour couvrir sa responsabilité civile ;<br>
                                4° Les cotisations facultatives des salariés de l’entreprise dont le comité d’entreprise fixe éventuellement les conditions de perception et les effets (il faut comprendre les paiements des salariés pour pouvoir bénéficier des œuvres sociales). Les décrets ne tiennent pas compte de cet élément pour le calcul des 153.000 €. Par contre,  pour les gros CE devant faire certifier les comptes, ces éléments sont à prendre en compte ;<br>
                                5° Les subventions accordées par les collectivités publiques ou les organisations syndicales ;<br>
                                6° Les dons et legs ;<br>
                                7° Les recettes procurées par les manifestations organisées par le comité ;<br>
                                8° Les revenus des biens meubles et immeubles du comité.<br>
                            </i>
                        </div>
                        <p>Il faut comprendre qu’en plus des subventions, il faut ajouter ces différents types de ressources financières.</p>
                        <p>Tout comme si une partie de la subvention est reversée à un CCE : il convient de ne pas en tenir compte au niveau de l’établissement et du CCE. Il ne faudra en tenir compte qu’au niveau du CCE. Par contre, les CCE et les établissements devront signer un accord de transfert de gestion qui va cadrer les échanges financiers, les montants, les obligations respectives, la durée qui risque d’être inférieure à la durée d’un mandat…).</p>
                        <div class="content-block frame-shadow-raised text-center">
                            <h3>FAITE DONC APPEL A NOUS POUR TOUT LES TRAVAUX COMPTABLES DE VOTRE COMITE</h3>
                            <button class="btn btn-lg btn-primary" onclick="window.location='index.php?view=contact'">CONTACTEZ-NOUS !!</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section("footer_scripts")

@stop    