@extends('template')
@section("title")
    Logiciel de gestion et de comptabilité CE, CSE
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ env('APP_NAME') }}</a> </li>
                <li>Solution</li>
                <li class="active">@yield("title")</li>
            </ul>
        </div>
    </div>
    <section id="main" class="no-margin no-padding">
        <div class="full-width-box">
            <div class="fwb-bg paralax" data-stellar-background-ratio="-0.01" style="background-image: url('/assets/custom/images/srice_dashboard.png')"><div class="overlay"></div></div>
            <div class="container">
                <h1 class="title white text-center"><i class="livicon" data-name="desktop" data-size="90" data-color="#ffffff"></i> LOGICIEL DE GESTION & COMPTABILITE CE, CSE</h1>
            </div>
        </div>

        <div class="full-width-box no-padding no-margin">
            <div class="fwb-bg cm-gradient">
                <div class="overlay"></div>
            </div>

            <div class="container">
                <div class="title-box text-center title-white">
                    <h2 class="h1 title">Notre Solution de Gestion & de Comptabilité</h2>
                </div>

                <p class="text-center white">Gérez votre comité Sociales & Economique facilement et gagnez du temps avec une plateforme tout-en-un intégrant la comptabilité et la gestion.</p>

                <div class="row services white">
                    <div class="service col-sm-4 col-md-4" data-appear-animation="bounceInLeft">
                        <a href="#">
                            <div class="icon border">
                                <div class="livicon" data-n="users" data-s="42" data-c="#fff" data-hc="0"></div>
                            </div>
                            <h6 class="title">Bénéficiaire</h6>
                            <div class="text-small">Notre module de gestion des bénéficiaires, vos salariés et leurs Ayants Droits sont insérer et maintenue quotidiennement en toutes séreinité.</div>
                        </a>
                    </div>

                    <div class="service col-sm-4 col-md-4" data-appear-animation="bounceInDown">
                        <a href="#">
                            <div class="icon border">
                                <div class="livicon" data-n="piechart" data-s="42" data-c="#fff" data-hc="0"></div>
                            </div>
                            <h6 class="title">Gestion des oeuvres Sociales</h6>
                            <div class="text-small">Le module de gestions des Oeuvres Sociales permet de gérer quotidiennement et avec simplicité votre billetterie, les remboursements, l'attribution des chèques ANCV et des chèques Cadeaux mais également tous évènement à votre convenance avec <strong>EventCreator &trade;</strong></div>
                        </a>
                    </div>

                    <div class="service col-sm-4 col-md-4" data-appear-animation="bounceInRight">
                        <a href="#">
                            <div class="icon border">
                                <div class="livicon" data-n="barchart" data-s="42" data-c="#fff" data-hc="0" data-d="1600"></div>
                            </div>
                            <h6 class="title">Comptabilité des Oeuvres Sociales</h6>
                            <div class="text-small">Le module de comptabilité des Oeuvres Sociales permet d'executer toutes les taches relative aux Oeuvres Sociales (Remise de chèque, produit et charges fixes, etc..) et d'avoir un aperçu statistique de l'état de vos Oeuvres Sociales.</div>
                        </a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="service col-sm-4 col-md-4" data-appear-animation="bounceInLeft">
                        <a href="#">
                            <div class="icon border">
                                <div class="livicon" data-n="piggybank" data-s="42" data-c="#fff" data-hc="0"></div>
                            </div>
                            <h6 class="title">Comptabilité du budget de fonctionnement</h6>
                            <div class="text-small">La Comptabilité du budget de fonctionnement doit ce faire avec une certaine rigueur dans le comité et nous le savons, c'est pourquoi, le module est ergonomique et vous permet d'acceder à l'ensemble des documents relatif à votre comptabilité</div>
                        </a>
                    </div>

                    <div class="service col-sm-4 col-md-4" data-appear-animation="bounceInUp">
                        <a href="#">
                            <div class="icon border">
                                <div class="livicon" data-n="connect" data-s="42" data-c="#fff" data-hc="0" data-d="1600"></div>
                            </div>
                            <h6 class="title">Logiciel Connecter</h6>
                            <div class="text-small">Vous n'avez rien à installer, le logiciel est disponible en accès par authentification par le web.</div>
                        </a>
                    </div>

                    <div class="service col-sm-4 col-md-4" data-appear-animation="bounceInRight">
                        <a href="#">
                            <div class="icon border">
                                <div class="livicon" data-n="server" data-s="42" data-c="#fff" data-hc="0"></div>
                            </div>
                            <h6 class="title">LinkSaver &trade;</h6>
                            <div class="text-small">LinkSaver &trade; est un module dédié à la sauvegarde inhérante de vos données sur des serveurs sécurisées toutes les heures.<br>Aucun souci de perte de données ou de fuites puisque les sauvegardes sont découpés en plusieurs morceaux et sauvegarder sur 5 serveurs distants des serveurs d'origine.</div>
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div><!-- .full-width-box -->
        <div class="full-width-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="title">Vous êtes intéresser ?</h2>
                        <h5 class="subtitle grey">Contactez-nous afin de de bénéficier de 15 Jours d'essai gratuitement</h5>
                    </div>
                    <div class="col-md-4 text-center vertical">
                        <button class="btn btn-lg btn-success" onclick="window.location='/contact'"><i class="fa fa-envelope-square"></i> Contactez-nous</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section("footer_scripts")

@stop    