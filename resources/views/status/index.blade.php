@extends('template')
@section("title")
    Status des Services
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ env('APP_NAME') }}</a> </li>
                <li class="active">@yield('title')</li>
            </ul>
        </div>
    </div><!-- .breadcrumb-box -->
    <section class="main">
        <header class="page-header">
            <div class="container">
                <h1 class="title">@yield("title")</h1>
            </div>
        </header>
        <div class="container">
            <div class="panel-group" id="status">
                @foreach($projets as $projet)
                <div class="panel panel-primary active">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <a data-toggle="collapse" data-parent="#status" href="#{{ $projet->id }}">
                                {{ $projet->nameProjet }}
                            </a>
                        </div>
                    </div>
                    <div id="{{ $projet->id }}" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <?php foreach (\App\Http\Controllers\Status\StatusController::getCategories($projet->id) as $category): ?>
                            <div class="title-box">
                                <h2 class="title">{{ $category->nameCategorie }}</h2>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Type</th>
                                            <th>Categorie</th>
                                            <th>Sujet</th>
                                            <th>Ouvert le</th>
                                            <th>Etat</th>
                                            <th>Modifié le</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach (\App\Http\Controllers\Status\StatusController::getTasks($category->id) as $task): ?>
                                        <?php
                                          $dateStart = strtotime($task->created_at);
                                          $created_at = \Carbon\Carbon::createFromTimestamp($dateStart)->format('d/m/Y à H:i');

                                        $updatedStart = strtotime($task->updated_at);
                                        $updated_at = \Carbon\Carbon::createFromTimestamp($updatedStart)->format('d/m/Y à H:i');
                                        ?>
                                        <tr>
                                            <td>{{ $task->id }}</td>
                                            <td>{{ \App\Http\Controllers\Status\StatusController::getTypeName($task->types_id) }}</td>
                                            <td>{{ \App\Http\Controllers\Status\StatusController::getCategorieName($category->id) }}</td>
                                            <td>{{ $task->titleTask }}</td>
                                            <td>{{ $created_at }}</td>
                                            <td>{!! \App\Http\Controllers\Status\StatusController::getEtatTaskLabel($task->statusTask) !!}</td>
                                            <td>{{ $updated_at }}</td>
                                            <td>
                                                <a href="{{ route('status.show', $task->id) }}" class="btn btn-xs btn-icon btn-primary"><i class="fa fa-eye"></i> </a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@stop
@section("footer_scripts")

@stop    