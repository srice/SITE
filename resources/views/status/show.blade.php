@extends('template')
@section("title")
    {{ $task->titleTask }}
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ env('APP_NAME') }}</a> </li>
                <li>Statut</li>
                <li class="active">@yield('title')</li>
            </ul>
        </div>
    </div><!-- .breadcrumb-box -->
    <section class="main">
        <header class="page-header">
            <div class="container">
                <h1 class="title">{{ \App\Http\Controllers\Status\StatusController::getCategorieName($task->categories_id) }} - @yield("title")</h1>
            </div>
        </header>
        <div class="container">
            <div class="panel panel-default">
                <header class="panel-heading">
                    <div class="row">
                        <div class="col-md-10">
                            <h3 class="panel-title">{{ $task->titleTask }}</h3>
                        </div>
                        <div class="col-md-2 text-right">{!! \App\Http\Controllers\Status\StatusController::getEtatTaskLabel($task->statusTask) !!}</div>
                    </div>
                </header>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Projet</td>
                                <td>{{ \App\Http\Controllers\Status\StatusController::getProjetName($task->categories_id) }}</td>
                            </tr>
                            <tr>
                                <td>Catégorie</td>
                                <td>{{ \App\Http\Controllers\Status\StatusController::getCategorieName($task->categories_id) }}</td>
                            </tr>
                            <tr>
                                <td>Type d'intervention</td>
                                <td>{{ \App\Http\Controllers\Status\StatusController::getTypeName($task->types_id) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <hr class="shadow" />
                    <strong>Description de la tache:</strong><br>
                    <br>
                    {!! $task->descTask !!}
                </div>
            </div>
            @if(\App\Http\Controllers\Status\StatusController::countTimeline($task->id) != 0)
                <div class="panel panel-default">
                    <header class="panel-heading">
                        <h3 class="panel-title">Mise à jours du status</h3>
                    </header>
                    <div class="panel-body">
                        <div class="timeline">
                            @foreach($timelines as $timeline)
                                <?php
                                $date = strtotime($timeline->created_at);
                                $created_at = \Carbon\Carbon::createFromTimestamp($date)->format('d/m/Y à H:i');
                                ?>
                                <article class="post">
                                    <div class="timeline-time">
                                        <time datetime="{{ $created_at }}">{{ $created_at }}</time>
                                    </div>

                                    <div class="timeline-icon">
                                        <div class="livicon" data-n="comments" data-c="#fff" data-hc="0" data-s="22"></div>
                                    </div>

                                    <div class="timeline-content" data-appear-animation="fadeInLeft">
                                        <h2 class="entry-title">
                                            <a href="">Nouveau Commentaire de la part de SRICE SUPPORT</a>
                                        </h2>

                                        <div class="entry-content">
                                            {!! $timeline->descTimeline !!}
                                        </div>
                                    </div>
                                </article><!-- .post -->
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
@stop
@section("footer_scripts")

@stop    