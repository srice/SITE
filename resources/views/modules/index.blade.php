@extends('template')
@section("title")
    Nos Modules
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ env('APP_NAME') }}</a> </li>
                <li class="active">@yield("title")</li>
            </ul>
        </div>
    </div>
    <section id="main">
        <header class="page-header">
            <div class="container">
                <h1 class="title">Nos Modules</h1>
            </div>
        </header>
        <div class="container">
            <div class="row text-center">
                @foreach($modules as $module)
                <div class="col-sm-3 col-md-3 product">
                    <div class="default">
                        {!! \App\Http\Controllers\Modules\ModuleOtherController::cornerRelease($module->release) !!}
                        <a href="{{ route('modules.show', $module->id) }}" class="product-image">
                            <img class="replace-2x" src="//{{ env("APP_GEST_DOMAIN") }}/storage/media/img/module/{{ $module->image }}" alt="" title="" width="270" height="270">
                        </a>
                        <div class="product-description">
                            <div class="vertical">
                                <h3 class="product-name">
                                    <a href="{{ route('modules.show', $module->id) }}">{{ $module->designation }}</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@stop
@section("footer_scripts")

@stop    