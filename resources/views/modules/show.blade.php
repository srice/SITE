@extends('template')
@section("title")
    {{ $module->designation }}
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ env('APP_NAME') }}</a> </li>
                <li><a href="{{ route('modules.index') }}">Nos modules</a></li>
                <li class="active">@yield("title")</li>
            </ul>
        </div>
    </div>
    <section id="main">
        <header class="page-header">
            <div class="container">
                <h1 class="title">@yield('title')</h1>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <article class="content product-page col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-sm-5 col-md-5">
                            <div class="image-box">
                                {!! \App\Http\Controllers\Modules\ModuleOtherController::cornerRelease($module->release) !!}
                                <div class="general-img">
                                    <img class="replace-2x" alt="" src="//{{ env("APP_GEST_DOMAIN") }}/storage/media/img/module/{{ $module->image }}" data-zoom-image="//{{ env("APP_GEST_DOMAIN") }}/storage/media/img/module/{{ $module->image }}" width="700" height="700">
                                </div><!-- .general-img -->
                            </div>
                        </div>

                        <div class="col-sm-7 col-md-7">
                            <div class="description">
                                {!! $module->description !!}
                            </div>

                            <div class="price-box">
                                <span class="price">
                                    @if($module->release >= 5)
                                        Veuillez nous consulter
                                    @else
                                        En Développement
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="product-tab">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#description">Description</a></li>
                        </ul><!-- .nav-tabs -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="description">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>Version du module</td>
                                            <td>{{ $module->version }}</td>
                                        </tr>
                                        <tr>
                                            <td>Release du module</td>
                                            <td>{{ \App\Http\Controllers\Modules\ModuleOtherController::getNameRelease($module->release) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .tab-content -->
                    </div>

                    <div class="clearfix"></div>
                </article><!-- .content -->
            </div>
        </div>
    </section>
@stop
@section("footer_scripts")

@stop    