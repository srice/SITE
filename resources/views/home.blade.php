@extends('template')
@section('title')
    Bienvenue
    @parent
@stop
@section("header_styles")

@stop

@section("content")
    <div class="slider rs-slider load">
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    <li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
                        <div class="elements">

                            <!--<h2 class="tp-caption lft skewtotop title"
                                data-x="722"
                                data-y="101"
                                data-speed="1000"
                                data-start="1700"
                                data-easing="Power4.easeOut"
                                data-endspeed="500"
                                data-endeasing="Power1.easeIn"
                                style="color: black;">
                                <strong class="bg-grey">TABLEAU DE BORD REACTIF</strong>
                            </h2>

                            <div class="tp-caption lfr skewtoright description"
                                 data-x="707"
                                 data-y="189"
                                 data-speed="1000"
                                 data-start="1500"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power1.easeIn"
                                 style="max-width: 480px; color: black">
                                <p class="bg-grey">Suivez l'avancer de vos stocks de prestation !</p>
                            </div>-->

                            <!--<a href="http://themeforest.net/item/progressive-multipurpose-responsive-template/7197521"
                               class="btn cherry tp-caption lfb btn-default"
                               data-x="722"
                               data-y="342"
                               data-speed="1000"
                               data-start="1700"
                               data-easing="Power4.easeOut"
                               data-endspeed="500"
                               data-endeasing="Power1.easeIn">
                                Read more
                            </a>-->
                        </div>

                        <img src="/assets/custom/images/slider/srice.png" alt="" data-bgfit="cover" data-bgposition="center" data-bgrepeat="no-repeat">
                    </li>
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>
    </div><!-- .rs-slider -->
    <div class="full-width-box">
        <div class="fwb-bg cm-gradient"><div class="overlay"></div></div>

        <div class="container">
            <div class="row">
                <div class="content-box col-sm-6 col-md-6">
                    <h1 class="title light white">LOGICIEL DE GESTION & DE COMPTABILITÉ POUR LES COMITÉS D'ENTREPRISE, CSE ET COLLECTIVITÉS</h1>
                    <h3 class="subtitle light white">SOLUTION BILLETTERIE & AUTRES SERVICES DEDIES SANS AUCUN ENGAGEMENT DE LONGUE DURÉE</h3>
                    <p class="description white">
                        Toutes nos solutions pour les / CE / CSE /COS et MAIRIES sont élaborés sur un contrat engagement d'1 seule année reconductible par tacite reconduction, contrat dénonçable à la volonté et du bon vouloir de changement de prestataire par le client par LR AR 3 mois avant la date d'anniversaire. Nous travaillons sur nos valeurs et notre savoir-faire en s'engageant à vos côtés.
                    </p>
                    <h4 class="white">SERVICES DÉDIÉS:</h4>
                    <p class="description white">
                        Nous travaillons en étroite collaboration avec nos partenaires spécialisés en formations, bilan comptable, mission légale et autres besoins pour les élus CE CSE. Nous attestons de la qualité des services apportés par notre logiciel SRICE de Gestion Compta. Nous sommes proches des élus du fait des services que nous apportons à l'ensemble de nos clients.
                    </p>
                    <a class="btn btn-red btn-default" href="https://logiciel.srice.eu">Accédez à la démo</a>
                    <br><br>
                </div>
                <div class="images-box col-sm-6 col-md-6">
                    <img src="/assets/custom/images/srice_dashboard.png">
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <section id="main">
        <article class="content">
            <div class="full-width-box">
                <div class="container-fluid" style="background-color: #10c1c5; padding-top: 20px; padding-bottom: 20px;">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="title">RESABILLETCSE</h2>
                            <h4 class="subtitle">PLATEFORME EUROPEENNE D’AVANTAGES SOCIAUX CE - CSE - COLLECTIVITES - MAIRIES - TPE - PME - PMI</h4>
                            <p class="text-danger h5">SOLUTION BILLETTERIE &amp; AUTRES SERVICES DEDIES SANS AUCUN ENGAGEMENT DE LONGUE DURÉE</p>
                            <p>Toutes nos solutions pour les / CE / CSE /COS et MAIRIES sont élaborés sur un contrat engagement d&#39;1 seule année reconductible
                                par tacite reconduction, contrat dénonçable à la volonté et du bon vouloir de changement de prestataire par le client par LR AR 3
                                mois avant la date d&#39;anniversaire.
                                Nous travaillons sur nos valeurs et notre savoir-faire en s&#39;engageant à vos côtés</p>
                        </div>
                        <div class="col-md-4 text-center vertical">
                            <button class="btn btn-lg btn-success" onclick="window.location='https://resabilletcse.com'"><i class="fa fa-shopping-cart"></i> Découvrez notre billetterie</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="text-center">
                    <h1>Participer <strong>activement</strong> à la vie de votre CE, CSE avec SRICE.</h1>
                </div>
            </div>
            <div class="container">
                <div class="row bottom-padding">
                    <div class="big-services-box col-md-4 col-sm-3">
                        <a href="#">
                            <div class="big-icon border border-info">
                                <i class="livicon" data-name="desktop" data-size="62" data-color="#0098CA"></i>
                            </div>
                            <h4 class="title">Logiciel de Gestion et de Comptabilité CSE</h4>
                            <span class="text-small">Conçu pour gérer votre CE, CSE de A à Z</span>
                        </a>
                    </div>
                    <div class="big-services-box col-md-4 col-sm-3">
                        <a href="#">
                            <div class="big-icon border border-warning">
                                <i class="livicon" data-name="shopping-cart" data-size="62" data-color="#F89406"></i>
                            </div>
                            <h4 class="title">Billetterie & Shopping</h4>
                            <span class="text-small">Un site e-commerce complet proposant des avantages et réductions pour les salariés.</span>
                        </a>
                    </div>
                    <div class="big-services-box col-md-4 col-sm-3">
                        <a href="#">
                            <div class="big-icon border border-success">
                                <i class="livicon" data-name="cellphone" data-size="62" data-color="#738D00"></i>
                            </div>
                            <h4 class="title">Site Internet & Appli CE, CSE</h4>
                            <span class="text-small">Des outils de communication à la pointe de la technologie pour votre CE, CSE</span>
                        </a>
                    </div>
                    <!--<div class="big-services-box col-md-3 col-sm-3">
                        <a href="#">
                            <div class="big-icon border border-error">
                                <i class="livicon" data-name="balance" data-size="62" data-color="red"></i>
                            </div>
                            <h4 class="title">Assistance Juridique & Comptable (AJC)</h4>
                            <span class="text-small">Service d'information et de protection pour les salariés et CE.</span>
                        </a>
                    </div>-->
                </div>
            </div>

            <div class="full-width-box">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="title">Télécharger notre plaquette</h2>
                        </div>
                        <div class="col-md-4 text-center vertical">
                            <button class="btn btn-lg btn-success" onclick="window.location='//{{ env('APP_DOMAIN') }}/assets/download/plaquette.pdf'"><i class="fa fa-file-pdf-o"></i> Plaquette Commercial</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row bottom-padding">
                    <div class="col-md-12 text-center">
                        <h3>Notre logiciel et nos services respecte et se base sur la loi du 27 mars 2015, relatif aux obligations comptable des comités Sociaux et Economiques. <a href="https://www.legifrance.gouv.fr/eli/decret/2015/3/27/ETST1431378D/jo/texte">Plus d'info...</a></h3>
                    </div>
                </div>
            </div>
            <!--<div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="bottom-padding">
                            <div class="carousel-box load" data-carousel-pagination="true">
                                <div class="title-box">
                                    <a class="next" href="#">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
                                        <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="1,0.001 0,1.001 7,8 0,14.999 1,15.999 9,8 "></polygon>
                                    </svg>
                                    </a>
                                    <a class="prev" href="#">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
                                        <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="8,15.999 9,14.999 2,8 9,1.001 8,0.001 0,8 "></polygon>
                                    </svg>
                                    </a>
                                    <h2 class="title">Derniers Post</h2>
                                </div>
                                <div class="post carousel row no-responsive">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <img src="http://placehold.it/855x624" alt="" title="" width="855" height="624" style="width: 370px;">
                                        </div>
                                        <div class="col-xs-12 col-sm-8 col-md-8">
                                            <h2 class="entry-title">Override des Serveurs 1 & 2 !</h2>
                                            <div class="entry-content">
                                                <p>Lundi 21 Novembre 2016, une attaque de grande envergure à eu lieu sur les serveurs 1 & 2 du parc SRICE.<br>Malgré les assaults DDoS Et OaX mener, les services techniques ont réussi à contrecarer cette attaque en overridant les serveurs impacter, Anhilan toutes récidives de la part de ces attaquant.</p>
                                                <p>Mais en quoi sa consiste exactement ?</p>
                                                <div class="text-right">
                                                    <button class="btn btn-primary">En savoir plus</button>
                                                </div>
                                            </div>
                                            <div class="entry-meta">
                                                <span class="autor-name">Mockelyn Maxime</span>,
                                                <span class="time"> 21.11.2016</span>
                                                <span class="separator">|</span>
                                                <span class="meta">Poster dans <a href="#">Logiciel</a>, <a href="#">ACK</a></span>
                                                <span class="comments-link pull-right"><a href="">3 Commentaires</a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <img src="http://placehold.it/855x624" alt="" title="" width="855" height="624" style="width: 370px;">
                                        </div>
                                        <div class="col-xs-12 col-sm-8 col-md-8">
                                            <h2 class="entry-title">Override des Serveurs 1 & 2 !</h2>
                                            <div class="entry-content">
                                                <p>Lundi 21 Novembre 2016, une attaque de grande envergure à eu lieu sur les serveurs 1 & 2 du parc SRICE.<br>Malgré les assaults DDoS Et OaX mener, les services techniques ont réussi à contrecarer cette attaque en overridant les serveurs impacter, Anhilan toutes récidives de la part de ces attaquant.</p>
                                                <p>Mais en quoi sa consiste exactement ?</p>
                                                <div class="text-right">
                                                    <button class="btn btn-primary">En savoir plus</button>
                                                </div>
                                            </div>
                                            <div class="entry-meta">
                                                <span class="autor-name">Mockelyn Maxime</span>,
                                                <span class="time"> 21.11.2016</span>
                                                <span class="separator">|</span>
                                                <span class="meta">Poster dans <a href="#">Logiciel</a>, <a href="#">ACK</a></span>
                                                <span class="comments-link pull-right"><a href="">3 Commentaires</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pagination switches"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </article>
    </section><!-- #main -->
@stop

@section("footer_scripts")

@stop