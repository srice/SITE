@extends('template')
@section("title")
    NOS PARTENAIRES
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route("home") }}">{{ env('APP_NAME') }}</a> </li>
                <li>Solution</li>
                <li class="active">@yield("title")</li>
            </ul>
        </div>
    </div>
    <section id="main">
        <div class="full-width-box">
            <div class="fwb-bg paralax" data-stellar-background-ratio="-0.01" style="background-image: url('/assets/custom/images/web.jpg')"><div class="overlay"></div></div>
            <div class="container">
                <h1 class="title white text-center"><i class="livicon" data-name="users" data-size="90" data-color="#ffffff"></i> Nos Partenaires</h1>
            </div>
        </div>
        <div class="container">
            <div class="title-box">
                <h2 class="title">Ceux qui nous font confiance:</h2>
            </div>
            <div class="banner-set load bottom-padding" data-autoplay-disable="true">
                <div class="container">
                    <div class="banners">
                        <a href="#" class="banner">
                            <img class="replace-2x" src="/assets/custom/images/partenaires/commerciaux/dassault.jpg" width="253" height="158" alt="">
                            <h2 class="title">Dassault Aviation</h2>
                            <div class="description">Le groupe Dassault Aviation est un constructeur aéronautique français fondé en 1929 par Marcel Bloch et reste le dernier groupe d'aviation au monde détenu par la famille de son fondateur et portant son nom.</div>
                        </a>
                        <a href="#" class="banner">
                            <img class="replace-2x" src="/assets/custom/images/partenaires/commerciaux/rexam.png" width="253" height="158" alt="">
                            <h2 class="title">REXAM</h2>
                            <div class="description">Rexam PLC (LSE : REX [archive]) est leader dans le domaine des emballages en plastique et de la production de canettes de boissons.</div>
                        </a>
                        <a href="#" class="banner">
                            <img class="replace-2x" src="/assets/custom/images/partenaires/commerciaux/samsic.jpg" width="253" height="158" alt="">
                            <h2 class="title">SAMSIC</h2>
                            <div class="description">Samsic est une société française de services aux entreprises</div>
                        </a>
                        <a href="#" class="banner">
                            <img class="replace-2x" src="/assets/custom/images/partenaires/commerciaux/leroy.png" width="253" height="158" alt="">
                            <h2 class="title">LEROY MERLIN</h2>
                            <div class="description">Leroy Merlin est une enseigne de grande distribution française spécialisée dans la construction, le bricolage et le jardinage.</div>
                        </a>
                        <a href="#" class="banner">
                            <img class="replace-2x" src="/assets/custom/images/partenaires/commerciaux/amaris.png" width="253" height="158" alt="">
                            <h2 class="title">AMARIS</h2>
                            <div class="description">Amaris est un groupe international de conseil en management et technologies</div>
                        </a>
                        <a href="#" class="banner">
                            <img class="replace-2x" src="/assets/custom/images/partenaires/commerciaux/Eiffage.jpg" width="253" height="158" alt="">
                            <h2 class="title">EIFFAGE</h2>
                            <div class="description">Eiffage est un groupe de construction et de concessions français</div>
                        </a>
                    </div><!-- .banners -->
                    <div class="clearfix"></div>
                </div>
                <div class="nav-box">
                    <div class="container">
                        <a class="prev" href="#"><span class="glyphicon glyphicon-arrow-left"></span></a>
                        <div class="pagination switches"></div>
                        <a class="next" href="#"><span class="glyphicon glyphicon-arrow-right"></span></a>
                    </div>
                </div>
            </div><!-- .banner-set -->
        </div>
        <div class="container">
            <div class="title-box">
                <h2 class="title">Partenaires Associatifs</h2>
            </div>
            <div class="banner-set load bottom-padding" data-autoplay-disable="true">
                <div class="container">
                    <div class="banners">
                        <a href="#" class="banner">
                            <img class="replace-2x" src="/assets/custom/images/partenaires/associatif/scpvs.png" width="300" alt="">
                            <h2 class="title">SCPVS</h2>
                            <div class="description">SCPVS (Solution Chirurgie Pathologie Vital Santé) est une assosications d'accompagnement pour les pathologies.</div>
                        </a>

                    </div><!-- .banners -->
                    <div class="clearfix"></div>
                </div>
                <div class="nav-box">
                    <div class="container">
                        <a class="prev" href="#"><span class="glyphicon glyphicon-arrow-left"></span></a>
                        <div class="pagination switches"></div>
                        <a class="next" href="#"><span class="glyphicon glyphicon-arrow-right"></span></a>
                    </div>
                </div>
            </div><!-- .banner-set -->
        </div>
    </section>
@stop
@section("footer_scripts")

@stop    