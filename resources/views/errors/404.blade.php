<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>OOOH ! | SRICE</title>
    <meta name="keywords" content="SRICE, Comité, ce, gestion, comptabilité, fonctionnement, reseau, saas, logiciel, oeuvre social, budget de fonctionnement, budget, oeuvre, social">
    <meta name="description" content="Srice est un logiciel de gestion et de comptabilité pour les comités d'entreprise en ligne: Gérez votre billetterie, votre Comptabilité de fonctionnement et d'autre chose simplement !">
    <meta name="author" content="SRICE">
    <meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/assets/img/favicon.ico">

    <!-- Font -->
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic'>

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="/assets/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/jslider.css">
    <link rel="stylesheet" href="/assets/css/revslider/settings.css">
    <link rel="stylesheet" href="/assets/css/jquery.fancybox.css">
    <link rel="stylesheet" href="/assets/css/animate.css">
    <link rel="stylesheet" href="/assets/css/video-js.min.css">
    <link rel="stylesheet" href="/assets/css/morris.css">
    <link rel="stylesheet" href="/assets/css/royalslider/royalslider.css">
    <link rel="stylesheet" href="/assets/css/royalslider/skins/minimal-white/rs-minimal-white.css">
    <link rel="stylesheet" href="/assets/css/layerslider/css/layerslider.css">
    <link rel="stylesheet" href="/assets/css/ladda.min.css">
    <link rel="stylesheet" href="/assets/css/datepicker.css">
    <link rel="stylesheet" href="/assets/css/jquery.scrollbar.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="/assets/css/style.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/assets/css/customizer/pages.css">
    <link rel="stylesheet" href="/assets/css/customizer/pages-pages-customizer.css">

    <!-- IE Styles-->
    <link rel='stylesheet' href="/assets/css/ie/ie.css">

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel='stylesheet' href="/assets/css/ie/ie8.css">
    <![endif]-->
</head>
<body class="page-404-promo blur-page" data-blur-image="/assets/content/img/band-4.jpg" data-blur-amount="0.5">

<div class="page-box">
    <section id="main">
        <div class="content-404">
            <header class="title">
                <h1>404</h1>
            </header>
            <div class="content">
                <h2>Vous êtes Perdu !</h2>
                <a href="{{ route('home') }}" class="btn btn-lg btn-white back-home"><i class="fa fa-home"></i> Retour à la page d'accueil</a>
            </div>
        </div>
    </section><!-- #main -->
</div><!-- .page-box -->

<!--[if (!IE)|(gt IE 8)]><!-->
<script src="/assets/js/jquery-3.0.0.min.js"></script>
<!--<![endif]-->

<!--[if lte IE 8]>
<script src="/assets/js/jquery-1.9.1.min.js"></script>
<![endif]-->
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/pixastic.custom.js"></script>
<script src="/assets/js/main.js"></script>

</body>
</html>