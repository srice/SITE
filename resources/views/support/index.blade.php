@extends('template')
@section("title")
    Support
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="slider rs-slider load">
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    <li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
                        <div class="elements">

                            <h2 class="tp-caption lft skewtotop title"
                                data-x="0"
                                data-y="180"
                                data-speed="1000"
                                data-start="1700"
                                data-easing="Power4.easeOut"
                                data-endspeed="500"
                                data-endeasing="Power1.easeIn"
                                style="color: black;">
                                <strong>Besoin d'aide<br>ou d'informations ?</strong>
                            </h2>

                            <div class="tp-caption lfr skewtotop description"
                                 data-x="0"
                                 data-y="300"
                                 data-speed="1000"
                                 data-start="1500"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power1.easeIn"
                                 style="max-width: 480px; color: black">
                                <p>Vous trouverez toutes les réponses à vos questions grâce à notre support dédié.</p>
                            </div>

                            <div class="tp-caption"
                                 data-x="500"
                                 data-y="0"
                                 data-speed="1000"
                                 data-start="1500"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power1.easeIn"
                                 style="max-width: 480px; color: black">
                                <img src="/assets/content/img/slider/callcenter-2.png" width="750" alt="">
                            </div>

                            <!--<a href="http://themeforest.net/item/progressive-multipurpose-responsive-template/7197521"
                               class="btn cherry tp-caption lfb btn-default"
                               data-x="722"
                               data-y="342"
                               data-speed="1000"
                               data-start="1700"
                               data-easing="Power4.easeOut"
                               data-endspeed="500"
                               data-endeasing="Power1.easeIn">
                                Read more
                            </a>--->
                        </div>

                        <img src="/assets/content/img/slider/transparent.png" alt="" style="background-color: rgba(225,225,225,0.55)" data-bgfit="cover" data-bgposition="center" data-bgrepeat="no-repeat">
                    </li>
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <nav class="navbar navbar-default center-block">
            <div class="container">

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{{ route('support.index') }}">Accueil <span class="sr-only">(current)</span></a></li>
                        <li><a href="{{ route('support.contact') }}">Contactez-nous</a></li>
                        <li><a href="//docs.{{ env('APP_DOMAIN') }}">Guides</a></li>
                        <li><a href="{{ route('status.index') }}">Statut</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
    <div class="full-width-box">
        <div class="fwb-bg bg-info"><div class="overlay"></div></div>

        <div class="container">
            <div class="row">
                <h2 class="text-center" style="color: #fff;">Découvrer nos guides</h2>
                <div class="content-box padding-bottom frame border-radius col-sm-6 col-md-6">
                    <div class="row">
                        <div class="col-md-6 text-center" style="vertical-align: middle">
                            <div style="margin-top: 15px; text-align: center; color: #fff;">
                                <i class="fa fa-users fa-5x"></i><br>
                                <h3 class="title">Bénéficiaires & Ayant Droit</h3>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-unstyled" style="margin-top: 15px; font-weight: bold; font-size: 14px; color: #fff;">
                                <li><a href="">Configuration</a></li>
                                <li><a href="">Gestion des Bénéficiaires</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="content-box padding-bottom frame border-radius col-sm-6 col-md-6">
                    <div class="row">
                        <div class="col-md-6 text-center" style="vertical-align: middle">
                            <div style="margin-top: 15px; text-align: center; color: #fff;">
                                <i class="fa fa-shopping-cart fa-5x"></i><br>
                                <h3 class="title">Gestion des Oeuvres Sociales</h3>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-unstyled" style="margin-top: 15px; font-weight: bold; font-size: 14px; color: #fff;">
                                <li><a href="">Configuration</a></li>
                                <li><a href="">Prestations</a></li>
                                <li><a href="">Vente de Billetterie</a></li>
                                <li><a href="">Remboursement</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" style="margin-top: 30px;">
                <a href="//docs.{{ env('APP_DOMAIN') }}" class="btn btn-warning">Voir l'ensemble de nos guides</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="banner-set load bottom-padding" data-autoplay-disable="true">
            <div class="container">
                <div class="banners">
                    <a href="{{ route('status.index') }}" class="banner">
                        <span class="livicon metro-bg" data-n="wrench" data-s="32" data-c="#ffffff" data-hc="false" data-op="1" style="background:#E51400;"></span>
                        <div style="margin-top: 10px;">
                            <h2 class="title text-center" style="font-weight: bold">Statut</h2>
                            <div class="description">Suivez l'état et les travaux des services SRICE</div>
                        </div>
                    </a>
                    <a href="{{ route('support.contact') }}" class="banner">
                        <span class="livicon metro-bg" data-n="phone" data-s="32" data-c="#ffffff" data-hc="false" data-op="1" style="background:#0ea70c;"></span>
                        <div style="margin-top: 10px;">
                            <h2 class="title text-center" style="font-weight: bold">Contactez</h2>
                            <div class="description">L'équipe de SRICE vous accompagne par mail ou par téléphone</div>
                        </div>
                    </a>
                    <a href="//docs.{{ env('APP_DOMAIN') }}" class="banner">
                        <span class="livicon metro-bg" data-n="help" data-s="32" data-c="#ffffff" data-hc="false" data-op="1" style="background:#0987a7;"></span>
                        <div style="margin-top: 10px;">
                            <h2 class="title text-center" style="font-weight: bold">Guides</h2>
                            <div class="description">Notre documentation complete sur le logiciel et les services annexes</div>
                        </div>
                    </a>
                </div><!-- .banners -->
                <div class="clearfix"></div>
            </div>
            <div class="nav-box">
                <div class="container">
                    <a class="prev" href="#"><span class="glyphicon glyphicon-arrow-left"></span></a>
                    <div class="pagination switches"></div>
                    <a class="next" href="#"><span class="glyphicon glyphicon-arrow-right"></span></a>
                </div>
            </div>
        </div><!-- .banner-set -->
    </div>
@stop
@section("footer_scripts")

@stop    