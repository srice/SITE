@extends('template')
@section("title")
    Nous Contacter
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="slider rs-slider load">
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    <li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
                        <div class="elements">

                            <h2 class="tp-caption lft skewtotop title"
                                data-x="0"
                                data-y="180"
                                data-speed="1000"
                                data-start="1700"
                                data-easing="Power4.easeOut"
                                data-endspeed="500"
                                data-endeasing="Power1.easeIn"
                                style="color: black;">
                                <strong>Besoin d'aide<br>ou d'informations ?</strong>
                            </h2>

                            <div class="tp-caption lfr skewtotop description"
                                 data-x="0"
                                 data-y="300"
                                 data-speed="1000"
                                 data-start="1500"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power1.easeIn"
                                 style="max-width: 480px; color: black">
                                <p>Vous trouverez toutes les réponses à vos questions grâce à notre support dédié.</p>
                            </div>

                            <div class="tp-caption"
                                 data-x="500"
                                 data-y="0"
                                 data-speed="1000"
                                 data-start="1500"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="500"
                                 data-endeasing="Power1.easeIn"
                                 style="max-width: 480px; color: black">
                                <img src="/assets/content/img/slider/callcenter-2.png" width="750" alt="">
                            </div>

                            <!--<a href="http://themeforest.net/item/progressive-multipurpose-responsive-template/7197521"
                               class="btn cherry tp-caption lfb btn-default"
                               data-x="722"
                               data-y="342"
                               data-speed="1000"
                               data-start="1700"
                               data-easing="Power4.easeOut"
                               data-endspeed="500"
                               data-endeasing="Power1.easeIn">
                                Read more
                            </a>--->
                        </div>

                        <img src="/assets/content/img/slider/transparent.png" alt="" style="background-color: rgba(225,225,225,0.55)" data-bgfit="cover" data-bgposition="center" data-bgrepeat="no-repeat">
                    </li>
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <nav class="navbar navbar-default center-block">
            <div class="container">

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ route('support.index') }}">Accueil <span class="sr-only">(current)</span></a></li>
                        <li class="active"><a href="{{ route('support.contact') }}">Contactez-nous</a></li>
                        <li><a href="{{ route('guide.index') }}">Guides</a></li>
                        <li><a href="{{ route('status.index') }}">Statut</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="bottom-padding col-sm-6 col-md-12">
                <div class="title-box text-center">
                    <h1 class="title">Pour nous contacter, rien de plus simple !</h1>
                </div>

                <div class="panel-group one-open" id="accordion">
                    <div class="panel panel-default active">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    Nouvelle commande
                                </a>
                            </div>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="text-center">
                                    <i>
                                        Besoin d’aide pour réaliser votre commande ?<br>
                                        Les équipes de SRICE vous accompagne par email ou par téléphone
                                    </i>
                                    <br><br>
                                    <span style="font-size: 45px;font-weight: bold;">0 899 492 648</span><br>
                                    <p>Joignez nos conseillers via un numéro unique et gratuit</p>
                                    <hr>
                                    <p>Pour une nouvelle commande: <span class="label label-info">Taper 1</span></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    Conseils Technique
                                </a>
                            </div>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="text-center">
                                    <i>
                                        Besoin d’aide pour réaliser votre commande ?<br>
                                        Les équipes de SRICE vous accompagne par email ou par téléphone
                                    </i>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 frame border-radius">
                                        <div class="text-center">
                                            <br><br>
                                            <span style="font-size: 45px;font-weight: bold;">0 899 492 648</span><br>
                                            <p>Joignez nos conseillers via un numéro unique et gratuit</p>
                                            <hr>
                                            <p>Pour une question technique: <span class="label label-info">Taper 2</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 frame border-radius">
                                        <div class="text-center">
                                            <br><br>
                                            <span style="font-size: 45px;font-weight: bold;"><i class="fa fa-envelope-square"></i></span><br>
                                            <p>Nos conseillers vous répondrons dans les meilleurs délais</p>
                                            <hr>
                                            <p>Contactez-nous par e-mail</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 frame border-radius">
                                        <div class="text-center">
                                            <br><br>
                                            <span style="font-size: 45px;font-weight: bold;"><i class="fa fa-twitter-square"></i></span><br>
                                            <p>Posez toutes vos questions à nos conseillers</p>
                                            <hr>
                                            <p>Suivez @srice_logiciel_support</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    Incidents
                                </a>
                            </div>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="text-center">
                                    <i>
                                        Votre site web n'est plus accessible ? Votre serveur ne répond plus ?<br>
                                        Entrez en contact avec des experts SRICE par téléphone ou en ouvrant un ticket incident.
                                    </i>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 frame border-radius">
                                        <div class="text-center">
                                            <br><br>
                                            <span style="font-size: 45px;font-weight: bold;">0 899 492 648</span><br>
                                            <p>Joignez nos conseillers via un numéro unique et gratuit</p>
                                            <hr>
                                            <p>Pour un incident: <span class="label label-info">Taper 3</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 frame border-radius">
                                        <div class="text-center">
                                            <br><br>
                                            <span style="font-size: 45px;font-weight: bold;"><i class="fa fa-envelope-square"></i></span><br>
                                            <p>Nos conseillers vous répondrons dans les meilleurs délais</p>
                                            <hr>
                                            <p>Contactez-nous par e-mail</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
@section("footer_scripts")

@stop    