<!--<div id="top-box">
    <div class="top-box-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-9 col-sm-5">
                    &nbsp;
                </div>

                <div class="col-xs-3 col-sm-7">
                    <div class="navbar navbar-inverse top-navbar top-navbar-right" role="navigation">
                        <button type="button" class="navbar-toggle btn-navbar collapsed" data-toggle="collapse" data-target=".top-navbar .navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <nav class="collapse collapsing navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="https://account.{{ env('APP_DOMAIN') }}"><i class="fa fa-user"></i> Mon Compte</a></li>
                                <li><a href="{{ route('support.index') }}"><i class="fa fa-life-ring"></i> Support</a></li>
                                <li><a href="//docs.{{ env('APP_DOMAIN') }}"><i class="fa fa-book"></i> Documentation</a></li>
                                <li class="bg-danger"><a href="//account.{{ env("APP_DOMAIN") }}/register-test"><i class="fa fa-key"></i> Demander ma période de test</a> </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>--><!-- #top-box -->
<header class="header header-two">
    <div class="header-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-2 col-lg-3 logo-box">
                    <div class="logo">
                        <a href="{{ route('home') }}">
                            <img src="/assets/img/logo.png" class="logo-img" alt="">
                        </a>
                    </div>
                </div><!-- .logo-box -->

                <div class="col-xs-6 col-md-10 col-lg-9 right-box">
                    <div class="right-box-wrapper">
                        <div class="header-icons">
                            <div class="phone-header hidden-600">
                                <a href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">
                                            <path d="M11.001,0H5C3.896,0,3,0.896,3,2c0,0.273,0,11.727,0,12c0,1.104,0.896,2,2,2h6c1.104,0,2-0.896,2-2
                                            c0-0.273,0-11.727,0-12C13.001,0.896,12.105,0,11.001,0z M8,15c-0.552,0-1-0.447-1-1s0.448-1,1-1s1,0.447,1,1S8.553,15,8,15z
                                            M11.001,12H5V2h6V12z"></path>
                                        <image src="/assets/img/png-icons/phone-icon.png" alt="" width="16" height="16" style="vertical-align: top;" >
				                            </svg>
                                </a>
                            </div><!-- .phone-header -->
                        </div><!-- .header-icons -->

                        <div class="primary">
                            <div class="navbar navbar-default" role="navigation">
                                <button type="button" class="navbar-toggle btn-navbar collapsed" data-toggle="collapse" data-target=".primary .navbar-collapse">
                                    <span class="text">Bienvenue</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                                <nav class="collapse collapsing navbar-collapse">
                                    <ul class="nav navbar-nav navbar-center">
                                        <li><a href="{{ route("home") }}">Accueil</a></li>
                                        <li class="parent megamenu promo">
                                            <a href="#">Solutions</a>
                                            <ul class="sub">
                                                <li class="sub-wrapper">
                                                    <div class="sub-list">
                                                        <div class="box">
                                                            <ul>
                                                                <li>
                                                                    <div class="panel bg-info" style="color: white;">
                                                                        <div class="panel-body">
                                                                            <div class="row">
                                                                                <div class="col-md-3">
                                                                                    <i class="livicon" data-name="desktop" data-size="55" data-color="#fff"></i>
                                                                                </div>
                                                                                <div class="col-md-9">
                                                                                    <h5>Logiciel de Gestion & de Comptabilité CE</h5>
                                                                                </div>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <a class="btn btn-border btn-block btn-inverse" href="{{ route("solution.logiciel") }}">
                                                                                    <i class="livicon" data-name="angle-right" data-size="12" data-color="#ffffff"></i> Découvrir
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="box">
                                                            <ul>
                                                                <li>
                                                                    <div class="panel bg-warning" style="color: white;">
                                                                        <div class="panel-body">
                                                                            <div class="row">
                                                                                <div class="col-md-3">
                                                                                    <i class="livicon" data-name="shopping-cart" data-size="55" data-color="#fff"></i>
                                                                                </div>
                                                                                <div class="col-md-9">
                                                                                    <h5>Billetterie CE <br>& Shopping</h5>
                                                                                </div>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <a class="btn btn-border btn-block btn-inverse" href="{{ route("solution.billetterie") }}">
                                                                                    <i class="livicon" data-name="angle-right" data-size="12" data-color="#ffffff"></i> Découvrir
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="box">
                                                            <ul>
                                                                <li>
                                                                    <div class="panel bg-success" style="color: white;">
                                                                        <div class="panel-body">
                                                                            <div class="row">
                                                                                <div class="col-md-3">
                                                                                    <i class="livicon" data-name="cellphone" data-size="55" data-color="#fff"></i>
                                                                                </div>
                                                                                <div class="col-md-9">
                                                                                    <h5>Site Internet <br>& Appli CE</h5>
                                                                                </div>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <a class="btn btn-border btn-block btn-inverse" href="{{ route("solution.apps") }}">
                                                                                    <i class="livicon" data-name="angle-right" data-size="12" data-color="#ffffff"></i> Découvrir
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <!--<div class="box">
                                                            <ul>
                                                                <li>
                                                                    <div class="panel bg-grey" style="color: black;">
                                                                        <div class="panel-body">
                                                                            <div class="row">
                                                                                <div class="col-md-3">
                                                                                    <i class="livicon" data-name="balance" data-size="55" data-color="#000"></i>
                                                                                </div>
                                                                                <div class="col-md-9">
                                                                                    <h5>Assistance Juridique & Comptable</h5>
                                                                                </div>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <button class="btn btn-border btn-block btn-inverse" onclick="window.location='/solution/compta'">
                                                                                    <i class="livicon" data-name="angle-right" data-size="12" data-color="#000"></i> Découvrir
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>-->
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <!--<li><a href="{{ route("blog.index") }}">Actualité</a></li>-->
                                        <li><a href="{{ route('partenaire.index') }}">Nos Partenaires</a></li>
                                        <!--<li><a href="{{ route('modules.index') }}">Modules</a></li>-->
                                        <li><a href="{{ route("contact.index") }}">Nous Contacter</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div><!-- .primary -->
                    </div>
                </div>

                <div class="phone-active col-sm-9 col-md-9">
                    <a href="#" class="close"><span>close</span>×</a>
                    <span class="title">Nous Contacter</span> <strong>0899 492 648</strong>
                </div>
            </div><!--.row -->
        </div>
    </div><!-- .header-wrapper -->
</header><!-- .header -->
