@extends('template')
@section("title")
    {{ $blog->titleNews }}
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route("home") }}">{{ env('APP_NAME') }}</a> </li>
                <li><a href="{{ route("blog.index") }}">Blog</a> </li>
                <li class="active">@yield("title")</li>
            </ul>
        </div>
    </div><!-- .breadcrumb-box -->

    <section id="main">
        <header class="page-header">
            <div class="container">
                <h1 class="title">{!! \App\Http\Controllers\Blog\BlogController::categorie($blog->categories_id) !!} - @yield('title')</h1>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <div class="content blog blog-post col-sm-9 col-md-12">
                    <article class="post">
                        <div class="entry-content">
                            <img class="img-responsive" src="https://new.{{ env('APP_DOMAIN') }}/assets/custom/img/blog/header/{{ $blog->id }}.png" alt="">
                            <hr class="shadow">
                            {!! $blog->descNews !!}
                        </div>
                        <footer class="entry-meta">
                            <span class="autor-name">{{ $blog->authorNews }}</span>,
                            <span class="time">{{ $blog->published_at }}</span>
                            <!--<span class="meta">Posted in <a href="#">Sports</a>, <a href="#">Movies</a></span>-->
                        <!--<span class="comments-link pull-right">
                              <a href="#">3 comment(s)</a>
                            </span>-->
                        </footer>
                    </article><!-- .post -->
                </div><!-- .content -->
            </div>
        </div><!-- .container -->
    </section><!-- #main -->
@stop
@section("footer_scripts")

@stop    