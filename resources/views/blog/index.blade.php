@extends('template')
@section("title")
    Blog
    @parent
@stop
@section("header_styles")

@stop
@section("content")
    <div class="breadcrumb-box">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ env('APP_NAME') }}</a> </li>
                <li class="active">@yield('title')</li>
            </ul>
        </div>
    </div><!-- .breadcrumb-box -->
    <section id="main">
        <header class="page-header">
            <div class="container">
                <h1 class="title">@yield("title")</h1>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <div class="content blog col-sm-9 col-md-12">
                    @foreach($blogs as $blog)
                        <?php
                          $date = $blog->published_at;
                          $strt = strtotime($date);
                          $published_at = \Carbon\Carbon::createFromTimestamp($strt)->format('d/m/Y');
                        ?>
                    <article class="post">
                        <div class="row">
                            <div class="col-md-2">
                                @if($blog->thumbnail == 1)
                                    <img class="replace-2x image img-rounded" src="https://new.{{ env('APP_DOMAIN') }}/assets/custom/img/blog/thumbnail/{{ $blog->id }}.png" alt width="150" height="150">
                                @else
                                    <img class="replace-2x image img-rounded" src="http://placehold.it" alt width="150" height="150">
                                @endif
                            </div>
                            <div class="col-md-10">
                                <h2 class="entry-title"><a href="{{ route('blog.post', $blog->id) }}">{{ $blog->titleNews }}</a> {!! \App\Http\Controllers\Blog\BlogController::categorie($blog->categories_id) !!}</h2>
                                <div class="entry-content">
                                    {!! str_limit($blog->miniDesc, 100, '...') !!}
                                </div>
                                <footer class="entry-meta">
                                    <span class="autor-name">{{ $blog->authorNews }}</span>,
                                    <span class="time">{{ $published_at }}</span>
                            </span>
                                </footer>
                            </div>
                        </div>
                    </article><!-- .post -->
                    @endforeach
                </div><!-- .content -->
            </div>
        </div><!-- .container -->
    </section><!-- #main -->
@stop
@section("footer_scripts")

@stop    