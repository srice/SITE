<h1>Nouveau typage de contact en provenance du site</h1>
<table>
    <tbody>
    <tr>
        <td>Nom:</td>
        <td>{{ $name }}</td>
    </tr>
    <tr>
        <td>Email:</td>
        <td>{{ $email }}</td>
    </tr>
    <tr>
        <td>Téléphone:</td>
        <td>{{ $tel }}</td>
    </tr>
    <tr>
        <td>Message:</td>
        <td>{{ $comment }}</td>
    </tr>
    </tbody>
</table>