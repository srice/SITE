<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>@section('title')
            | SRICE
        @show</title>
    <meta name="keywords" content="SRICE, Comité, cse, gestion, comptabilité, fonctionnement, reseau, saas, logiciel, oeuvre social, budget de fonctionnement, budget, oeuvre, social">
    <meta name="description" content="Srice est un logiciel de gestion et de comptabilité pour les comités Sociales et économiques en ligne: Gérez votre billetterie, votre Comptabilité de fonctionnement et d'autre chose simplement !">
    <meta name="author" content="SRICE">
    <meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/assets/img/favicon.ico">

    <!-- Font -->
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic'>

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="/assets/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/jslider.css">
    <link rel="stylesheet" href="/assets/css/revslider/settings.css">
    <link rel="stylesheet" href="/assets/css/jquery.fancybox.css">
    <link rel="stylesheet" href="/assets/css/animate.css">
    <link rel="stylesheet" href="/assets/css/video-js.min.css">
    <link rel="stylesheet" href="/assets/css/morris.css">
    <link rel="stylesheet" href="/assets/css/royalslider/royalslider.css">
    <link rel="stylesheet" href="/assets/css/royalslider/skins/minimal-white/rs-minimal-white.css">
    <link rel="stylesheet" href="/assets/css/layerslider/css/layerslider.css">
    <link rel="stylesheet" href="/assets/css/ladda.min.css">
    <link rel="stylesheet" href="/assets/css/datepicker.css">
    <link rel="stylesheet" href="/assets/css/jquery.scrollbar.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="/assets/css/style.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/assets/css/customizer/pages.css">
    <link rel="stylesheet" href="/assets/css/customizer/pages-pages-customizer.css">

    <!-- IE Styles-->
    <link rel='stylesheet' href="/assets/css/ie/ie.css">

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel='stylesheet' href="/assets/css/ie/ie8.css">
    <![endif]-->
</head>
<body class="fixed-header">
<div class="page-box">
    <div class="page-box-content">

        @include("includes.header")

        @yield("content")

    </div><!-- .page-box-content -->
</div><!-- .page-box -->

@include("includes.footer")
<div class="clearfix"></div>

<!--[if (!IE)|(gt IE 8)]><!-->
<script src="/assets/js/jquery-3.0.0.min.js"></script>
<!--<![endif]-->

<!--[if lte IE 8]>
<script src="/assets/js/jquery-1.9.1.min.js"></script>
<![endif]-->
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/price-regulator/jshashtable-2.1_src.js"></script>
<script src="/assets/js/price-regulator/jquery.numberformatter-1.2.3.js"></script>
<script src="/assets/js/price-regulator/tmpl.js"></script>
<script src="/assets/js/price-regulator/jquery.dependClass-0.1.js"></script>
<script src="/assets/js/price-regulator/draggable-0.1.js"></script>
<script src="/assets/js/price-regulator/jquery.slider.js"></script>
<script src="/assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="/assets/js/jquery.touchwipe.min.js"></script>
<script src="/assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="/assets/js/jquery.imagesloaded.min.js"></script>
<script src="/assets/js/jquery.appear.js"></script>
<script src="/assets/js/jquery.sparkline.min.js"></script>
<script src="/assets/js/jquery.easypiechart.min.js"></script>
<script src="/assets/js/jquery.easing.1.3.js"></script>
<script src="/assets/js/jquery.fancybox.pack.js"></script>
<script src="/assets/js/isotope.pkgd.min.js"></script>
<script src="/assets/js/jquery.knob.js"></script>
<script src="/assets/js/jquery.selectBox.min.js"></script>
<script src="/assets/js/jquery.royalslider.min.js"></script>
<script src="/assets/js/jquery.tubular.1.0.js"></script>
<script src="/assets/js/SmoothScroll.js"></script>
<script src="/assets/js/country.js"></script>
<script src="/assets/js/spin.min.js"></script>
<script src="/assets/js/ladda.min.js"></script>
<script src="/assets/js/masonry.pkgd.min.js"></script>
<script src="/assets/js/morris.min.js"></script>
<script src="/assets/js/raphael.min.js"></script>
<script src="/assets/js/video.js"></script>
<script src="/assets/js/pixastic.custom.js"></script>
<script src="/assets/js/livicons-1.4.min.js"></script>
<script src="/assets/js/layerslider/greensock.js"></script>
<script src="/assets/js/layerslider/layerslider.transitions.js"></script>
<script src="/assets/js/layerslider/layerslider.kreaturamedia.jquery.js"></script>
<script src="/assets/js/revolution/jquery.themepunch.tools.min.js"></script>
<script src="/assets/js/revolution/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
	(Load Extensions only on Local File Systems !
	The following part can be removed on Server for On Demand Loading) -->
<script src="/assets/js/revolution/extensions/revolution.extension.actions.min.js"></script>
<script src="/assets/js/revolution/extensions/revolution.extension.carousel.min.js"></script>
<script src="/assets/js/revolution/extensions/revolution.extension.kenburn.min.js"></script>
<script src="/assets/js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/assets/js/revolution/extensions/revolution.extension.migration.min.js"></script>
<script src="/assets/js/revolution/extensions/revolution.extension.navigation.min.js"></script>
<script src="/assets/js/revolution/extensions/revolution.extension.parallax.min.js"></script>
<script src="/assets/js/revolution/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/assets/js/revolution/extensions/revolution.extension.video.min.js"></script>
<script src="/assets/js/bootstrapValidator.min.js"></script>
<script src="/assets/js/bootstrap-datepicker.js"></script>
<script src="/assets/js/jplayer/jquery.jplayer.min.js"></script>
<script src="/assets/js/jplayer/jplayer.playlist.min.js"></script>
<script src="/assets/js/jquery.scrollbar.min.js"></script>
<script src="/assets/js/main.js"></script>
@yield('footer_scripts')

</body>
</html>