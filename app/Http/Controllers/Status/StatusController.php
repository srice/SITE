<?php

namespace App\Http\Controllers\Status;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class StatusController extends Controller
{
    public function index(){
        $projets = DB::connection('gestion')->table('status_projets')->get();

        return view('status.index', compact('projets'));
    }

    public function show($id){
        $task = DB::connection('gestion')->table('status_tasks')->where('id', $id)->first();
        $timelines = DB::connection('gestion')->table('status_task_timelines')->where('tasks_id', $task->id)->get();

        return view('status.show', compact('task', 'timelines'));
    }

    public static function getCategories($projet_id){
        $categories = DB::connection('gestion')->table('status_categories')->where('projets_id', $projet_id)->get();

        return $categories;
    }

    public static function getTasks($categories_id){
        $tasks = DB::connection('gestion')->table('status_tasks')->where('categories_id', $categories_id)->get();
        return $tasks;
    }

    public static function getCategorieName($categories_id){
        $categorie = DB::connection('gestion')->table('status_categories')->where('id', $categories_id)->first();
        return $categorie->nameCategorie;
    }

    public static function getTypeName($types_id){
        $type = DB::connection('gestion')->table('status_types')->where('id', $types_id)->first();
        return $type->nameType;
    }

    public static function getProjetName($categories_id){
        $categorie = DB::connection('gestion')->table('status_categories')->where('id', $categories_id)->first();
        $projet = DB::connection('gestion')->table('status_projets')->where('id', $categorie->projets_id)->first();
        return $projet->nameProjet;
    }

    public static function countTimeline($tasks_id){
        $timelines = DB::connection('gestion')->table('status_task_timelines')->where('tasks_id', $tasks_id)->get()->count();
        return $timelines;
    }

    public static function getEtatTaskLabel($value){
        switch ($value){
            case 0: return '<span class="label label-default">Planifier</span>';
            case 1: return '<span class="label label-warning">En cours...</span>';
            case 2: return '<span class="label label-success">Terminer</span>';
        }
    }
}
