<?php

namespace App\Http\Controllers\Support;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupportController extends Controller
{
    public function index(){
        return view('support.index');
    }

    public function contact(){
        return view('support.contact');
    }
}
