<?php

namespace App\Http\Controllers\Contact;

use App\Http\Requests\reCaptchataTestFormRequest;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index(){
        return view('contact.index');
    }

    public function store(Mailer $mailer, reCaptchataTestFormRequest $request){
        $data = $request->all();
        $mailer->send('email.contact', ["name" => $data['name'], "email" => $data["email"], "tel" => $data["tel"], "comment" => $data["comment"]], function($message){
            $message->to('contact@srice.eu')->subject('Nouvelle espace de contact');
        });

        if($mailer){
            return redirect()->route('contact.index')->with('success', 'Le mail à été envoyé avec succès !');
        }else{
            return redirect()->route('contact.index')->with('error', 'Erreur lors de l\'envoie du mail');
        }
    }
}
