<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index(){

        $blogs = DB::connection('gestion')->table('news')->where('published', 1)->latest()->get();

        return view('blog.index', compact('blogs'));
    }

    public function post($post_id){
        $blog = DB::connection('gestion')->table('news')->where('id', $post_id)->first();

        return view('blog.post', compact('blog'));
    }

    public static function categorie($id){
        $categorie = DB::connection('gestion')->table('categories')->where('id', $id)->first();

        return '<span class="label label-'.$categorie->color.'">'.$categorie->nameCategory.'</span>';
    }
}
