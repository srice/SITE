<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Analytics\AnalyticsFacade;
use Spatie\Analytics\Period;

class TestController extends Controller
{
    public function test(){
        $analyticsData = AnalyticsFacade::fetchVisitorsAndPageViews(Period::days(7));
        dd($analyticsData);
    }
}
