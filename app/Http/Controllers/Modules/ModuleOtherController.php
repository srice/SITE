<?php

namespace App\Http\Controllers\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleOtherController extends ModuleController
{
    public static function cornerRelease($value){
        switch ($value){
            case 0: return '<span class="sale alpha"></span>';
            case 1: return '<span class="sale beta"></span>';
            case 2: return '<span class="sale gamma"></span>';
            case 3: return '<span class="sale end"></span>';
        }
    }

    public static function getNameRelease($value){
        switch ($value){
            case 0: return 'ALPHA';
            case 1: return 'BETA';
            case 2: return 'GAMMA';
            case 3: return 'LIVE';
        }
    }

    public static function euro($value){
        return number_format($value, 2, ',', ' ')." €";
    }
}
