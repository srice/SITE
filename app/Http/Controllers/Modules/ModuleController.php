<?php

namespace App\Http\Controllers\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ModuleController extends Controller
{
    public function index(){
        $modules = DB::connection('gestion')->table('modules')->where('id', '>=', '6')->get();
        return view('modules.index', compact('modules'));
    }

    public function show($id){
        $module = DB::connection('gestion')->table('modules')->where('id', $id)->first();
        $service = DB::connection('gestion')->table('services')->where('modules_id', $id)->first();
        return view('modules.show', compact('module', 'service'));
    }
}
