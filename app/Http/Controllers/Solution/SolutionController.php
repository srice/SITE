<?php

namespace App\Http\Controllers\Solution;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SolutionController extends Controller
{
    public function logiciel(){
        return view('solution.logiciel');
    }

    public function billetterie(){
        return view('solution.billetterie');
    }

    public function apps(){
        return view('solution.apps');
    }

    public function compta(){
        return view('solution.compta');
    }
}
